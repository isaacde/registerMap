image: registry.gitlab.com/registermap/utility/registry-images/python-build:8.0.0

variables:
  FLIT_ROOT_INSTALL: 1

  PACKAGE_NAME: registerMap

  FLIT_USERNAME: SECURE
  FLIT_PASSWORD: SECURE

  CPP_IMAGE_PATH: registry.gitlab.com/registermap/utility/registry-images/gpp-build:0.1.0
    
    
stages:
  - build
  - test
  - deploy


.wheel-build: &build-wheel
  script:
    - '/venv/bin/flit build --format wheel'

  artifacts:
    paths:
      - dist/*.whl
      - ${PACKAGE_NAME}.egg-info/*


.sdist-build: &build-sdist
  script:
    - '/venv/bin/flit build --format sdist'

  artifacts:
    paths:
      - dist/*.tar.gz
      - ${PACKAGE_NAME}.egg-info/*


.restrict-feature: &feature-restricted
  only:
    - branches
    - master
  except:
    - tags


.restrict-tags: &tags-restricted
  only:
    # Semantic version tags https://semver.org
    - /^\d+\.\d+\.\d+$/
  except:
    - branches


.release-version: &acquire-tagged-version
  before_script:
    - echo ${CI_COMMIT_REF_NAME} > ${PACKAGE_NAME}/VERSION


.pipeline-version: &acquire-pipeline-version
  before_script:
    # Use the VERSION 0.0.0 for feature branches appended with the pipeline id.
    - echo "0.0.0.dev${CI_PIPELINE_ID}" > ${PACKAGE_NAME}/VERSION


build-wheel-package:
  stage: build

  <<: *acquire-pipeline-version
  <<: *build-wheel

  <<: *feature-restricted


build-wheel-release-package:
  stage: build

  <<: *acquire-tagged-version
  <<: *build-wheel

  <<: *tags-restricted


build-source-package:
  stage: build

  <<: *acquire-pipeline-version
  <<: *build-sdist

  <<: *feature-restricted


build-source-release-package:
  stage: build

  <<: *acquire-tagged-version
  <<: *build-sdist

  <<: *tags-restricted


python-unittests:
  stage: test

  before_script:
    # Ensure that dependencies are installed
    - '/venv/bin/flit install -s'
  script:
    - '/venv/bin/nosetests
         --logging-level=INFO'


python-coverage-report:
  stage: test

  before_script:
    # Ensure that dependencies are installed
    - '/venv/bin/flit install -s'
  script:
    - '/venv/bin/nosetests
          --with-xunit
          --with-cov
          --cov-config .coveragerc
          --logging-level=INFO
          --cov-report term-missing
          --cov-report html
          --cov-report xml
          --cov ${PACKAGE_NAME}'

  artifacts:
    paths:
      - nosetests.html
      - nosetests.xml
      - coverage.xml
      - htmlcov/*
    reports:
      junit: nosetests.xml
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'


wheel-install-test:
  stage: test
  dependencies:
    - build-wheel-package

  script:
    - '/venv/bin/pip3 install dist/${PACKAGE_NAME}*.whl'

  <<: *feature-restricted


source-install-test:
  stage: test
  dependencies:
    - build-source-package

  script:
    - '/venv/bin/pip3 install dist/${PACKAGE_NAME}*.tar.gz'

  <<: *feature-restricted


.exporter-prep: &prepare-exporter
  before_script:
    - '/venv/bin/pip3 install dist/${PACKAGE_NAME}*.whl'


cpp-exporter-tests:
  image: ${CPP_IMAGE_PATH}
  stage: test
  dependencies:
    - build-wheel-package

  script:
    - 'make -C testExport/cpp all'

  <<: [ *feature-restricted, *prepare-exporter ]


c-exporter-tests:
  # The C++ image does C as well and can be reused here.
  image: ${CPP_IMAGE_PATH}
  stage: test
  dependencies:
    - build-wheel-package

  script:
    - 'make -C testExport/c all'

  <<: [ *feature-restricted, *prepare-exporter ]


# http://samb.io/blog/2016/devops/python/gitlab/continuous-deployment/continuous-deployment-to-python-packaging
deploy-pypi:
  dependencies:
    - build-wheel-release-package
    - build-source-release-package
  stage: deploy

  <<: *acquire-tagged-version
  script:
    - '/venv/bin/flit publish'

  <<: *tags-restricted
